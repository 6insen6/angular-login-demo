import {Injectable, Output} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.baseUrl;

  //Здесь для примера используется boolean, в проде тут будет модель пользователя
  @Output() public isLoginned: boolean = false;

  constructor(private http: HttpClient) {
    this.isLoginned = localStorage.getItem('logined') == 'true';
  }

  login(credentials: any, statusCode: string): Observable<any> {
    return this.http.post<any>(this.baseUrl + statusCode, credentials)
      .pipe(
        map(user => {
          if (user) {
            localStorage.setItem('logined', 'true');
          }
          return user;
        }),
        catchError(
          e => {
            if (e.status === 400) {
            }
            return throwError(e);
          }
        )
      );
  }
}
