import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model: any = {}
  constructor(private accountService: AccountService,
              private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    //statusCode используется для симуляции ответа с сервера
    let statusCode = '401/cors';
    if (this.model.username === 'login' && this.model.password === 'pass')
      statusCode = '200/cors';
    this.accountService.login(this.model, statusCode).subscribe(
      res => {
        this.router.navigateByUrl('');
      }
    )
  }
}
